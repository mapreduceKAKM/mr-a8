#########################     README    ############################# 

-------------------------------------------------------
Assignment A8: REGRESSION USING SPARK
- By Kamlendra, Mayuri, Sri and Kavyashree 
-------------------------------------------------------

#NOTE: MAKE SURE YOU ARE IN THE “Code” FOLDER WHEN YOU RUN THE COMMANDS FROM TERMINAL

-------------------------------------------------------
1. Software
-------------------------------------------------------
 
* JDK version 1.8
* Hadoop version 2.7.1
* Apache Spark v.1.6.0
* AWS Command Line Interface 
* R, RStudio and all packages listed in 'R Configuration' section
* pandoc version 1.16.0.2 (Install from: http://pandoc.org/installing.html)
* LaTeX
* Maven

-------------------------------------------------------
2. Configuration for AWS
-------------------------------------------------------
 
* Configure the authentication keys using 'aws configure' command. 
* Make sure that the AWS credential file is present at ~/.aws/credentials'
* Make sure the default output format is text.

-------------------------------------------------------
3. Environment variables
-------------------------------------------------------
 
* Check that the following environment variables are set: $HADOOP_HOME, $JAVA_HOME $HADOOP_CLASSPATH, $R_HOME, $MAVEN_HOME, $SPARK_HOME are set

-------------------------------------------------------
4. Project
-------------------------------------------------------

The project file contains the following:
1. Makefile, which has the unix scripts to run the code
2. src which contains all the source code and jar files for the maven project
3. A8Report.Rmd which is used to generate report and graphs.

-------------------------------------------------------
5. Makefile
------------------------------------------------------- 
* Enter your input, output, models and all the required file paths in the Makefile.
* Enter your bucketname in the Makefile.

-------------------------------------------------------
6. Requirements:
-------------------------------------------------------
* The Scripts in the folder to run the code are suited for a Linux machine.
* In case, if you get the output already exists error: run this command ‘make del-prev-result’	
* ggplot2, sqldf, rmarkdown, knitr should be installed in R. Use the following commands to do so:
	install.packages("ggplot2")
	install.packages("rmarkdown")
	install.packages("sqldf")
	install.packages("knitr")


-------------------------------------------------------
7. Execution:
-------------------------------------------------------

Steps to run the project:
NOTE: We assume the user has installed Java, Hadoop, Scala and Spark before running the jobs.
1. Run the project in pseudo mode using : make pseudo
2. Run the project on the cloud using : make cloud