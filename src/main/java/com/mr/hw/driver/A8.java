package com.mr.hw.driver;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.storage.StorageLevel;

import com.mr.hw.MinCarrierFinder;
import com.mr.hw.DataReader;
import com.mr.hw.LinearRegressor;
import com.mr.hw.MedianFinder;
import com.mr.hw.flight.FlightDTO;

/**
 * Class that runs the various steps for the spark job
 * 
 * @author keskarm, annuraju, kamlendra, kavyashree
 *
 */
public class A8 {

	public static void main(String[] args) throws Exception {		
		if (args.length != 3) {
			System.err.println("Usage: A8 <input_path> <output_path> <N>");
			System.exit(-1);
		}
		
		SparkConf conf = new SparkConf().setAppName("SparkApplication");
		JavaSparkContext jsc = new JavaSparkContext(conf);
		
		JavaRDD<FlightDTO> filteredSaneFlights = DataReader.getFlightData(jsc, args[0]);
		filteredSaneFlights.persist(StorageLevel.MEMORY_AND_DISK());
		
		JavaRDD<String> predictions = LinearRegressor.predictPrice(filteredSaneFlights, Integer.parseInt(args[2]));
		
		String cheapestCarrier = MinCarrierFinder.findCheapestAirline(predictions);
		MedianFinder.findMedianPrice(jsc, filteredSaneFlights, cheapestCarrier, args[1]);
	}
}