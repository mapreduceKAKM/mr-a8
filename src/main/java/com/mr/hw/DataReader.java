package com.mr.hw;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;

import com.mr.hw.csvReader.CSVParser;
import com.mr.hw.exception.CorruptFlightDetailsException;
import com.mr.hw.flight.Flight;
import com.mr.hw.flight.FlightDTO;

/**
 * Generates the sane flight data as a Java RDD
 * @author annuraju, keskarm, kamlendra, kavyashree
 */
public class DataReader {

	/**
	 * Reads the data from input file. Creates the sane flight record's RDD.
	 * @param jsc
	 * @param input
	 * @return
	 */
	public static JavaRDD<FlightDTO> getFlightData(JavaSparkContext jsc, String input) {

		JavaRDD<String> lines = jsc.textFile(input);
		final CSVParser parser = new CSVParser(',', '"');

		/*
		* Converts the input record into and flight object and performs sanity test on it.
		*/
		JavaRDD<FlightDTO> flights = lines.map(new Function<String, FlightDTO>() {
			private static final long serialVersionUID = 1L;

			@Override
			public FlightDTO call(String line) throws Exception {
				String[] flightData = parser.parseLine(line);
				try {
					Flight flight = new Flight(flightData);
					return flight.isSaneFlight() ? new FlightDTO(flight) : null;
				} catch (CorruptFlightDetailsException | ArrayIndexOutOfBoundsException e) {
					return null; // Corrupt flight data
				}
			}
		});

		/*
		* returns true is a valid flight object else returns false
		*/
		JavaRDD<FlightDTO> filteredSaneFlights = flights.filter(new Function<FlightDTO, Boolean>() {
			private static final long serialVersionUID = 1L;

			@Override
			public Boolean call(FlightDTO flight) throws Exception {
				return flight != null;
			}
		});
		return filteredSaneFlights;
	}
}