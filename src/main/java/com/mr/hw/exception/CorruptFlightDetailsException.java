package com.mr.hw.exception;
/**
 * Exception class represents the invalid flight details.
 * @author Kamlendra, Mayuri, Kavya, Sri Annapurna
 *
 */
public class CorruptFlightDetailsException extends Exception {
  
	private static final long serialVersionUID = 1L;

	public CorruptFlightDetailsException() {
		super();
	}
	
	public CorruptFlightDetailsException(Throwable e) {
		super(e);
	}
}
