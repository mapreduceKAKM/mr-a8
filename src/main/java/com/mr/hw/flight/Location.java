package com.mr.hw.flight;

import java.io.Serializable;

import com.mr.hw.exception.CorruptFlightDetailsException;
import com.mr.hw.util.HWConstants;
import com.mr.hw.util.HWUtil;

/**
 * Represents the Airport location detail. Provides and abstraction for Origin
 * and Destination Airport details.
 * 
 * @author Kamlendra, Kavya, Mayuri, Sri Annapurna
 *
 */
public class Location implements Serializable {

	private static final long serialVersionUID = 1L;

	enum LocationType {
		ORIGIN, DESTINATION
	}

	private int airportId;
	private int airportSeqId;
	private int cityMarketId;
	private String airport;
	private String city;
	private String stateAbbr;
	private int stateFips;
	private String state;
	private int wac;

	public Location() {
	}

	public Location(String[] flightDetails, LocationType type) throws CorruptFlightDetailsException {
		switch (type) {
		case ORIGIN:
			populateOriginDetails(flightDetails);
			break;
		case DESTINATION:
			populateDestinationDetails(flightDetails);
		}
	}

	/**
	 * Populates the origin airport details.
	 * 
	 * @param flightDetails
	 * @throws CorruptFlightDetailsException
	 */
	private void populateOriginDetails(String[] flightDetails) throws CorruptFlightDetailsException {
		try {
			this.airportId = Integer.parseInt(flightDetails[HWConstants.ORIGIN_AIRPORT_ID]);
			this.airportSeqId = Integer.parseInt(flightDetails[HWConstants.ORIGIN_AIRPORT_SEQ_ID]);
			this.cityMarketId = Integer.parseInt(flightDetails[HWConstants.ORIGIN_CITY_MARKET_ID]);
			this.airport = flightDetails[HWConstants.ORIGIN];
			this.city = flightDetails[HWConstants.ORIGIN_CITY_NAME];
			this.stateAbbr = flightDetails[HWConstants.ORIGIN_STATE_ABR];
			this.stateFips = Integer.parseInt(flightDetails[HWConstants.ORIGIN_STATE_FIPS]);
			this.state = flightDetails[HWConstants.ORIGIN_STATE_NM];
			this.wac = Integer.parseInt(flightDetails[HWConstants.ORIGIN_WAC]);
		} catch (NumberFormatException ne) {
			throw new CorruptFlightDetailsException(ne);
		}
	}

	/**
	 * Populates the destination airport details.
	 * 
	 * @param flightDetails
	 * @throws CorruptFlightDetailsException
	 */
	private void populateDestinationDetails(String[] flightDetails) throws CorruptFlightDetailsException {
		try {
			this.airportId = Integer.parseInt(flightDetails[HWConstants.DEST_AIRPORT_ID]);
			this.airportSeqId = Integer.parseInt(flightDetails[HWConstants.DEST_AIRPORT_SEQ_ID]);
			this.cityMarketId = Integer.parseInt(flightDetails[HWConstants.DEST_CITY_MARKET_ID]);
			this.airport = flightDetails[HWConstants.DEST];
			this.city = flightDetails[HWConstants.DEST_CITY_NAME];
			this.stateAbbr = flightDetails[HWConstants.DEST_STATE_ABR];
			this.stateFips = Integer.parseInt(flightDetails[HWConstants.DEST_STATE_FIPS]);
			this.state = flightDetails[HWConstants.DEST_STATE_NM];
			this.wac = Integer.parseInt(flightDetails[HWConstants.DEST_WAC]);
		} catch (NumberFormatException ne) {
			throw new CorruptFlightDetailsException(ne);
		}
	}

	/**
	 * Checks if the location detail is valid.
	 * 
	 * @return true iff required fields are not empty and have values in correct
	 *         format.
	 */
	public boolean isValid() {
		boolean emptyValuesCheck = !HWUtil.isStringEmptyOrNull(airport) && !HWUtil.isStringEmptyOrNull(city)
				&& !HWUtil.isStringEmptyOrNull(stateAbbr) && !HWUtil.isStringEmptyOrNull(state);
		boolean validValueCheck = airportId > 0 && airportSeqId > 0 && cityMarketId > 0 && stateFips > 0 && wac > 0;
		return emptyValuesCheck && validValueCheck;
	}

	/**
	 * @return the airportId
	 */
	public int getAirportId() {
		return airportId;
	}

	/**
	 * @param airportId
	 *            the airportId to set
	 */
	public void setAirportId(int airportId) {
		this.airportId = airportId;
	}

	/**
	 * @return the airportSeqId
	 */
	public int getAirportSeqId() {
		return airportSeqId;
	}

	/**
	 * @param airportSeqId
	 *            the airportSeqId to set
	 */
	public void setAirportSeqId(int airportSeqId) {
		this.airportSeqId = airportSeqId;
	}

	/**
	 * @return the cityMarketId
	 */
	public int getCityMarketId() {
		return cityMarketId;
	}

	/**
	 * @param cityMarketId
	 *            the cityMarketId to set
	 */
	public void setCityMarketId(int cityMarketId) {
		this.cityMarketId = cityMarketId;
	}

	/**
	 * @return the airport
	 */
	public String getAirport() {
		return airport;
	}

	/**
	 * @param airport
	 *            the airport to set
	 */
	public void setAirport(String airport) {
		this.airport = airport;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the stateAbbr
	 */
	public String getStateAbbr() {
		return stateAbbr;
	}

	/**
	 * @param stateAbbr
	 *            the stateAbbr to set
	 */
	public void setStateAbbr(String stateAbbr) {
		this.stateAbbr = stateAbbr;
	}

	/**
	 * @return the stateFips
	 */
	public int getStateFips() {
		return stateFips;
	}

	/**
	 * @param stateFips
	 *            the stateFips to set
	 */
	public void setStateFips(int stateFips) {
		this.stateFips = stateFips;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the wac
	 */
	public int getWac() {
		return wac;
	}

	/**
	 * @param wac
	 *            the wac to set
	 */
	public void setWac(int wac) {
		this.wac = wac;
	}
}
