package com.mr.hw.flight;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.mr.hw.exception.CorruptFlightDetailsException;
import com.mr.hw.util.HWConstants;
import com.mr.hw.util.HWUtil;

/**
 * Represents the timecard of a flight record.
 * @author Kamlendra, Kavya, Mayuri, Sri Annapurna
 *
 */
public class FlightTimeDetails implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public static final int MISSING_VALUE = -99999;
	private static final int CANCELLED = 1;

	private int crsDepartureTime;
	private Calendar crsDepartureDate;
	
	private int crsArrivalTime;
	private Calendar crsArrivalDate;
	
	private int departureTime;
	private Calendar actualDepartureDate;
	
	private int arrivalTime;
	private Calendar actualArrivalDate;
	
	private double departureDelay;
	private double arrivalDelay;
	private double arrivalDelayNew;
	private double arrivalDelay15;

	private int crsElapsedTime;
	private int actualElapsedTime;

	private Boolean cancelled;
	private int timezone;
	
	public FlightTimeDetails() {
	}

	public FlightTimeDetails(String[] flightDetails) throws CorruptFlightDetailsException {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("HHmm");
			
			int year = Integer.parseInt(flightDetails[HWConstants.YEAR]);
			int month = Integer.parseInt(flightDetails[HWConstants.MONTH]);
			int day = Integer.parseInt(flightDetails[HWConstants.DAY_OF_MONTH]);
			
			// crs departure time
			crsDepartureTime = Integer.parseInt(flightDetails[HWConstants.CRS_DEP_TIME]);
			Date date = sdf.parse(checkTimeFormat(flightDetails[HWConstants.CRS_DEP_TIME]));
			int crsDepHour = date.getHours();
			int crsDepMinutes = date.getMinutes();
			int crsDepSeconds = date.getSeconds();
			crsDepartureDate = Calendar.getInstance();
			crsDepartureDate.set(year, month, day, crsDepHour, crsDepMinutes, crsDepSeconds);
			
			// crs arrival time
			crsArrivalTime = Integer.parseInt(flightDetails[HWConstants.CRS_ARR_TIME]);
			date = sdf.parse(checkTimeFormat(flightDetails[HWConstants.CRS_ARR_TIME]));
			int crsArrHour = date.getHours();
			int crsArrMinutes = date.getMinutes();
			int crsArrSeconds = date.getSeconds();
			
			crsArrivalDate = Calendar.getInstance();
			crsArrivalDate.set(year, month, day);
			if(crsArrHour < crsDepHour) {
				crsArrivalDate.add(Calendar.DAY_OF_MONTH, 1);
			}
			crsArrivalDate.set(Calendar.HOUR_OF_DAY, crsArrHour);
			crsArrivalDate.set(Calendar.MINUTE, crsArrMinutes);
			crsArrivalDate.set(Calendar.SECOND, crsArrSeconds);
			
			//actual departure time
			departureTime = getNumber(flightDetails[HWConstants.DEP_TIME]);
			date = sdf.parse(checkTimeFormat(flightDetails[HWConstants.DEP_TIME]));
			int depHour = date.getHours();
			int depMinutes = date.getMinutes();
			int depSeconds = date.getSeconds();
			actualDepartureDate = Calendar.getInstance();
			actualDepartureDate.set(year, month, day, depHour, depMinutes, depSeconds);
			
			// actual arrival time
			arrivalTime = getNumber(flightDetails[HWConstants.ARR_TIME]);
			date = sdf.parse(checkTimeFormat(flightDetails[HWConstants.ARR_TIME]));
			int arrHour = date.getHours();
			int arrMinutes = date.getMinutes();
			int arrSeconds = date.getSeconds();
			actualArrivalDate = Calendar.getInstance();
			actualArrivalDate.set(year, month, day);
			if(arrHour < depHour) {
				actualArrivalDate.add(Calendar.DAY_OF_MONTH, 1);
			}
			actualArrivalDate.set(Calendar.HOUR_OF_DAY, arrHour);
			actualArrivalDate.set(Calendar.MINUTE, arrMinutes);
			actualArrivalDate.set(Calendar.SECOND, arrSeconds);
			
			departureDelay = getDouble(flightDetails[HWConstants.DEP_DELAY]);
			
			arrivalDelay = getDouble(flightDetails[HWConstants.ARR_DELAY]);
			arrivalDelayNew = getDouble(flightDetails[HWConstants.ARR_DELAY_NEW]);
			arrivalDelay15 = getDouble(flightDetails[HWConstants.ARR_DEL15]);

			crsElapsedTime = Integer.parseInt(flightDetails[HWConstants.CRS_ELAPSED_TIME]);
			actualElapsedTime = getNumber(flightDetails[HWConstants.ACTUAL_ELAPSED_TIME]);

			cancelled = HWUtil.isStringEmptyOrNull(flightDetails[HWConstants.CANCELLED]) ? null
					: Integer.parseInt(flightDetails[HWConstants.CANCELLED]) == CANCELLED;

			timezone = convertIntoMinute(checkTimeFormat(flightDetails[HWConstants.CRS_ARR_TIME]))
					- convertIntoMinute(checkTimeFormat(flightDetails[HWConstants.CRS_DEP_TIME])) - crsElapsedTime;
		} catch (NumberFormatException | ParseException nfe) {
			throw new CorruptFlightDetailsException(nfe);
		}
	}

	private int getNumber(String value) {
		if (HWUtil.isStringEmptyOrNull(value)) {
			return MISSING_VALUE;
		}
		return Integer.parseInt(value);
	}

	private double getDouble(String value) {
		if (HWUtil.isStringEmptyOrNull(value)) {
			return MISSING_VALUE;
		}
		return Double.parseDouble(value);
	}

	/**
	 * Converts time string into minutes.
	 * 
	 * @param timeString
	 * @return
	 * @throws Exception
	 */
	private int convertIntoMinute(String timeString) throws CorruptFlightDetailsException {
		if (timeString.length() != 4) {
			System.out.println(timeString);
			throw new CorruptFlightDetailsException();
		}
		String hours = timeString.substring(0, 2);
		String minutes = timeString.substring(2);
		return Integer.parseInt(hours) * 60 + Integer.parseInt(minutes);
	}

	/**
	 * Checks if timeString if of form 040. Converts it to 0040.
	 * 
	 * @param timeString
	 * @return
	 */
	private String checkTimeFormat(String timeString) {
		if(timeString.length() == 3) {
			return "0".concat(timeString); 
		}
		if(timeString.length() == 2) {
			return "00".concat(timeString);
		}
		if(timeString.length() == 1) {
			return "000".concat(timeString);
		}
		return timeString;
	}

	/**
	 * Checks if the time detail of a flight is valid
	 * @return
	 * @throws CorruptFlightDetailsException
	 */
	public boolean isValid() throws CorruptFlightDetailsException {
		boolean isCRSDetailValid = validateCRSDetail();
		boolean isCancellationDetailValid = validateCancellation();
		return isCRSDetailValid && isCancellationDetailValid;

	}

	/**
	 * Checks of the cancellation details of a flight is valid.
	 * @return
	 * @throws CorruptFlightDetailsException
	 */
	private boolean validateCancellation() throws CorruptFlightDetailsException {
		if (cancelled == null || cancelled) {
			return true;
		}
		if (!isMissing(arrivalTime) && !isMissing(departureTime) && !isMissing(actualElapsedTime)) {
			boolean timecheck = (convertIntoMinute(checkTimeFormat(String.valueOf(arrivalTime)))
					- convertIntoMinute(checkTimeFormat(String.valueOf(departureTime))) - actualElapsedTime - timezone)
					% 24 == 0;
			boolean delayCheck = validateDelay();
			return timecheck && delayCheck;
		}
		return false;
	}

	/**
	 * Checks if the delay details of a flight is valid.
	 * @return
	 */
	private boolean validateDelay() {
		if (!isMissing(arrivalDelay) && !isMissing(arrivalDelayNew) && !isMissing(arrivalDelay15)) {
			if (arrivalDelay > 0) {
				if (arrivalDelay != arrivalDelayNew) {
					return false;
				}
				if (arrivalDelay >= 15 && arrivalDelay15 != 1) {
					return false;
				}
			} else {
				if (arrivalDelayNew != 0) {
					return false;
				}
			}
		} else {
			return false;
		}
		return true;
	}

	/**
	 * Validates the CRS time details of a flight.
	 * @return
	 */
	private boolean validateCRSDetail() {
		return crsArrivalTime > 0 && crsDepartureTime > 0 && (timezone % 60 == 0);
	}

	private boolean isMissing(int value) {
		return MISSING_VALUE == value;
	}

	private boolean isMissing(double value) {
		return MISSING_VALUE == value;
	}

	/**
	 * @return the crsDepartureTime
	 */
	public int getCrsDepartureTime() {
		return crsDepartureTime;
	}

	/**
	 * @param crsDepartureTime the crsDepartureTime to set
	 */
	public void setCrsDepartureTime(int crsDepartureTime) {
		this.crsDepartureTime = crsDepartureTime;
	}

	/**
	 * @return the departureTime
	 */
	public int getDepartureTime() {
		return departureTime;
	}

	/**
	 * @param departureTime the departureTime to set
	 */
	public void setDepartureTime(int departureTime) {
		this.departureTime = departureTime;
	}

	/**
	 * @return the crsArrivalTime
	 */
	public int getCrsArrivalTime() {
		return crsArrivalTime;
	}

	/**
	 * @param crsArrivalTime the crsArrivalTime to set
	 */
	public void setCrsArrivalTime(int crsArrivalTime) {
		this.crsArrivalTime = crsArrivalTime;
	}

	/**
	 * @return the arrivalTime
	 */
	public int getArrivalTime() {
		return arrivalTime;
	}

	/**
	 * @param arrivalTime the arrivalTime to set
	 */
	public void setArrivalTime(int arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	/**
	 * @return the arrivalDelay
	 */
	public double getArrivalDelay() {
		return arrivalDelay;
	}

	/**
	 * @param arrivalDelay the arrivalDelay to set
	 */
	public void setArrivalDelay(double arrivalDelay) {
		this.arrivalDelay = arrivalDelay;
	}

	/**
	 * @return the crsElapsedTime
	 */
	public int getCrsElapsedTime() {
		return crsElapsedTime;
	}

	/**
	 * @param crsElapsedTime the crsElapsedTime to set
	 */
	public void setCrsElapsedTime(int crsElapsedTime) {
		this.crsElapsedTime = crsElapsedTime;
	}

	/**
	 * @return the actualElapsedTime
	 */
	public int getActualElapsedTime() {
		return actualElapsedTime;
	}

	/**
	 * @param actualElapsedTime the actualElapsedTime to set
	 */
	public void setActualElapsedTime(int actualElapsedTime) {
		this.actualElapsedTime = actualElapsedTime;
	}

	/**
	 * @return the cancelled
	 */
	public Boolean getCancelled() {
		return cancelled;
	}

	/**
	 * @param cancelled the cancelled to set
	 */
	public void setCancelled(Boolean cancelled) {
		this.cancelled = cancelled;
	}

	/**
	 * @return the crsDepartureDate
	 */
	public Calendar getCrsDepartureDate() {
		return crsDepartureDate;
	}

	/**
	 * @param crsDepartureDate the crsDepartureDate to set
	 */
	public void setCrsDepartureDate(Calendar crsDepartureDate) {
		this.crsDepartureDate = crsDepartureDate;
	}

	/**
	 * @return the crsArrivalDate
	 */
	public Calendar getCrsArrivalDate() {
		return crsArrivalDate;
	}

	/**
	 * @param crsArrivalDate the crsArrivalDate to set
	 */
	public void setCrsArrivalDate(Calendar crsArrivalDate) {
		this.crsArrivalDate = crsArrivalDate;
	}

	/**
	 * @return the actualDepartureDate
	 */
	public Calendar getActualDepartureDate() {
		return actualDepartureDate;
	}

	/**
	 * @param actualDepartureDate the actualDepartureDate to set
	 */
	public void setActualDepartureDate(Calendar actualDepartureDate) {
		this.actualDepartureDate = actualDepartureDate;
	}

	/**
	 * @return the actualArrivalDate
	 */
	public Calendar getActualArrivalDate() {
		return actualArrivalDate;
	}

	/**
	 * @param actualArrivalDate the actualArrivalDate to set
	 */
	public void setActualArrivalDate(Calendar actualArrivalDate) {
		this.actualArrivalDate = actualArrivalDate;
	}

	/**
	 * @return the departureDelay
	 */
	public double getDepartureDelay() {
		return departureDelay;
	}

	/**
	 * @param departureDelay the departureDelay to set
	 */
	public void setDepartureDelay(double departureDelay) {
		this.departureDelay = departureDelay;
	}

}