/**
 * 
 */
package com.mr.hw.flight;

import java.io.Serializable;

/**
 * Lightweight Flight object.
 * @author kamlendra, annuraju, kavyashree, mayuri
 *
 */
public class FlightDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String airline;
	private int year;
	private int weekOfYear;
	private int crsElapsedTime;
	private double ticketPrice;
	
	public FlightDTO(Flight flight) {
		this.airline = flight.getAirline();
		this.year = flight.getYear();
		this.weekOfYear = flight.getWeekOfYear();
		this.crsElapsedTime = flight.getFlightTimeDetails().getCrsElapsedTime();
		this.ticketPrice = flight.getTicketPrice();
	}

	/**
	 * @return the airline
	 */
	public String getAirline() {
		return airline;
	}

	/**
	 * @param airline the airline to set
	 */
	public void setAirline(String airline) {
		this.airline = airline;
	}

	/**
	 * @return the year
	 */
	public int getYear() {
		return year;
	}

	/**
	 * @param year the year to set
	 */
	public void setYear(int year) {
		this.year = year;
	}

	/**
	 * @return the weekOfYear
	 */
	public int getWeekOfYear() {
		return weekOfYear;
	}

	/**
	 * @param weekOfYear the weekOfYear to set
	 */
	public void setWeekOfYear(int weekOfYear) {
		this.weekOfYear = weekOfYear;
	}

	/**
	 * @return the crsElapsedTime
	 */
	public int getCrsElapsedTime() {
		return crsElapsedTime;
	}

	/**
	 * @param crsElapsedTime the crsElapsedTime to set
	 */
	public void setCrsElapsedTime(int crsElapsedTime) {
		this.crsElapsedTime = crsElapsedTime;
	}

	/**
	 * @return the ticketPrice
	 */
	public double getTicketPrice() {
		return ticketPrice;
	}

	/**
	 * @param ticketPrice the ticketPrice to set
	 */
	public void setTicketPrice(double ticketPrice) {
		this.ticketPrice = ticketPrice;
	}
	
	@Override
	public String toString() {
		return this.airline + "," + this.year + "," + this.crsElapsedTime + "," + this.ticketPrice;
	}
}