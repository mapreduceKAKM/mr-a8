package com.mr.hw.flight;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import com.mr.hw.exception.CorruptFlightDetailsException;
import com.mr.hw.flight.Location.LocationType;
import com.mr.hw.util.HWConstants;

/**
 * Class representation of a flight record.
 * @author Kamlendra, Kavya, Mayuri, Sri Annapurna
 *
 */
public class Flight implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private int flightNumber;
	private int year;
	private int quarter;
	private int month;
	private int weekOfYear;
	private int day;
	private int dayOfWeek;
	private Date date;
	private String airline;
	private int airlineId;
	private int distance;
	
	private Location origin;
	private Location destination;
	
	private FlightTimeDetails flightTimeDetails;
	private double ticketPrice;
	
	public Flight() {
		origin = new Location();
		destination = new Location();
		flightTimeDetails = new FlightTimeDetails();
	}
	
	public Flight(String[] flightDetails) throws CorruptFlightDetailsException {
		try {
			flightNumber = Integer.parseInt(flightDetails[HWConstants.FL_NUM]);
			year = Integer.parseInt(flightDetails[HWConstants.YEAR]);
			quarter = Integer.parseInt(flightDetails[HWConstants.QUARTER]);
			month = Integer.parseInt(flightDetails[HWConstants.MONTH]);
			day = Integer.parseInt(flightDetails[HWConstants.DAY_OF_MONTH]);
			Calendar cal = Calendar.getInstance();
			cal.set(year, month, day);
			weekOfYear = cal.get(Calendar.WEEK_OF_YEAR);
			dayOfWeek = Integer.parseInt(flightDetails[HWConstants.DAY_OF_WEEK]);
			airlineId = Integer.parseInt(flightDetails[HWConstants.AIRLINE_ID]);
			airline = flightDetails[HWConstants.UNIQUE_CARRIER];
			origin = new Location(flightDetails, LocationType.ORIGIN);
			destination = new Location(flightDetails, LocationType.DESTINATION);
			flightTimeDetails = new FlightTimeDetails(flightDetails);
			ticketPrice = Double.parseDouble(flightDetails[HWConstants.AVG_TICKET_PRICE]);
			distance = Integer.parseInt(flightDetails[HWConstants.DISTANCE]);
		} catch (NumberFormatException nfe) {
			throw new CorruptFlightDetailsException(nfe);
		}
	}
	
	/**
	 * Checks if the flight details are valid.
	 * @return
	 * @throws CorruptFlightDetailsException
	 */
	public boolean isSaneFlight() throws CorruptFlightDetailsException {
		return origin.isValid() && destination.isValid() && flightTimeDetails.isValid();
	}

	/**
	 * @return the year
	 */
	public int getYear() {
		return year;
	}

	/**
	 * @param year the year to set
	 */
	public void setYear(int year) {
		this.year = year;
	}

	/**
	 * @return the month
	 */
	public int getMonth() {
		return month;
	}

	/**
	 * @param month the month to set
	 */
	public void setMonth(int month) {
		this.month = month;
	}

	/**
	 * @return the day
	 */
	public int getDay() {
		return day;
	}

	/**
	 * @param day the day to set
	 */
	public void setDay(int day) {
		this.day = day;
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return the airline
	 */
	public String getAirline() {
		return airline;
	}

	/**
	 * @param airline the airline to set
	 */
	public void setAirline(String airline) {
		this.airline = airline;
	}

	/**
	 * @return the airlineId
	 */
	public int getAirlineId() {
		return airlineId;
	}

	/**
	 * @param airlineId the airlineId to set
	 */
	public void setAirlineId(int airlineId) {
		this.airlineId = airlineId;
	}

	/**
	 * @return the origin
	 */
	public Location getOrigin() {
		return origin;
	}

	/**
	 * @param origin the origin to set
	 */
	public void setOrigin(Location origin) {
		this.origin = origin;
	}

	/**
	 * @return the destination
	 */
	public Location getDestination() {
		return destination;
	}

	/**
	 * @param destination the destination to set
	 */
	public void setDestination(Location destination) {
		this.destination = destination;
	}

	/**
	 * @return the flightTimeDetails
	 */
	public FlightTimeDetails getFlightTimeDetails() {
		return flightTimeDetails;
	}

	/**
	 * @param flightTimeDetails the flightTimeDetails to set
	 */
	public void setFlightTimeDetails(FlightTimeDetails flightTimeDetails) {
		this.flightTimeDetails = flightTimeDetails;
	}

	/**
	 * @return the ticketPrice
	 */
	public double getTicketPrice() {
		return ticketPrice;
	}

	/**
	 * @param ticketPrice the ticketPrice to set
	 */
	public void setTicketPrice(double ticketPrice) {
		this.ticketPrice = ticketPrice;
	}

	/**
	 * @return the flightNumber
	 */
	public int getFlightNumber() {
		return flightNumber;
	}

	/**
	 * @param flightNumber the flightNumber to set
	 */
	public void setFlightNumber(int flightNumber) {
		this.flightNumber = flightNumber;
	}

	/**
	 * @return the quarter
	 */
	public int getQuarter() {
		return quarter;
	}

	/**
	 * @param quarter the quarter to set
	 */
	public void setQuarter(int quarter) {
		this.quarter = quarter;
	}

	/**
	 * @return the dayOfWeek
	 */
	public int getDayOfWeek() {
		return dayOfWeek;
	}

	/**
	 * @param dayOfWeek the dayOfWeek to set
	 */
	public void setDayOfWeek(int dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}

	/**
	 * @return the distance
	 */
	public int getDistance() {
		return distance;
	}

	/**
	 * @param distance the distance to set
	 */
	public void setDistance(int distance) {
		this.distance = distance;
	}

	/**
	 * @return the weekOfYear
	 */
	public int getWeekOfYear() {
		return weekOfYear;
	}

	/**
	 * @param weekOfYear the weekOfYear to set
	 */
	public void setWeekOfYear(int weekOfYear) {
		this.weekOfYear = weekOfYear;
	}

}
