package com.mr.hw;

import org.apache.commons.math3.stat.regression.SimpleRegression;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFunction;

import com.mr.hw.flight.FlightDTO;
import com.mr.hw.util.HWConstants;

import scala.Tuple2;

/**
 * Predicts the price for each carrier in every year for a given N
 * @author keskarm, AnnuRaju, Kamlendra, Kavya
 */
public class LinearRegressor {

	/**
	 * Builds the regression model and predicts the price for a given N.
	 * @param filteredSaneFlights
	 * @param n
	 * @return
	 */
	public static JavaRDD<String> predictPrice(JavaRDD<FlightDTO> filteredSaneFlights, final int n) {

		JavaPairRDD<String, Iterable<String>> mapped = filteredSaneFlights
				.mapToPair(new PairFunction<FlightDTO, String, String>() {

					private static final long serialVersionUID = 1L;

					@Override
					public Tuple2<String, String> call(FlightDTO flight) throws Exception {
						String key = flight.getAirline() + HWConstants.SEPERATOR_COMMA + flight.getYear();
						String value = flight.getCrsElapsedTime() + HWConstants.SEPERATOR_COMMA
								+ flight.getTicketPrice();
						return new Tuple2<String, String>(key, value);
					}
				}).groupByKey();

		JavaRDD<String> predictions = mapped.map(new Function<Tuple2<String, Iterable<String>>, String>() {

			private static final long serialVersionUID = 1L;

			@Override
			public String call(Tuple2<String, Iterable<String>> tuple) throws Exception {
				SimpleRegression regression = new SimpleRegression(true);
				for (String jointValue : tuple._2()) {
					String[] values = jointValue.split(HWConstants.SEPERATOR_COMMA);
					int crsTime = Integer.parseInt(values[0]);
					double price = Double.parseDouble(values[1]);
					regression.addData(crsTime, price);
				}
				double prediction = regression.predict(n);
				return tuple._1() + HWConstants.SEPERATOR_COMMA + prediction;
			}
		});
		return predictions;
	}
}