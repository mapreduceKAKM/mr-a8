package com.mr.hw;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.apache.spark.api.java.JavaRDD;

import com.mr.hw.csvReader.CSVParser;

/**
 * Class to find the cheapest airline.
 * @author Kamlendra, keskarm, Kavya, Annnuraju
 */
public class MinCarrierFinder {

	/**
	 * Finds the cheapest airline.
	 * @param predictions
	 * @return String
	 * @throws IOException
	 */
	public static String findCheapestAirline(JavaRDD<String> predictions) throws IOException {
		Map<Integer, Double> yearMinPredictedPriceMap = new TreeMap<Integer, Double>();
		Map<Integer, String> yearCheapestAirlineMap = new TreeMap<Integer, String>();
		
		populateMaps(yearMinPredictedPriceMap, yearCheapestAirlineMap, predictions.collect());
		Map<String, Integer> airlineCountMap = populateCheapestAirlineCountMap(yearCheapestAirlineMap);

		return findCheapestAirline(airlineCountMap);
	}
	
	/**
	 * Takes a Map as input and finds the cheapest airline.
	 * @param airlineCountMap
	 * @return String
	 */
	private static String findCheapestAirline(Map<String, Integer> airlineCountMap) {
		String cheapestAirline = null;
		int maxFrequency = Integer.MIN_VALUE;
		for (Entry<String, Integer> entry : airlineCountMap.entrySet()) {
			String airline = entry.getKey();
			int frequency = entry.getValue();

			if (frequency > maxFrequency) {
				cheapestAirline = airline;
				maxFrequency = frequency;
			}
		}
		return cheapestAirline;
	}

	/**
	 * Takes a Map as input and returns a Map of airline with its corresponding count.
	 * @param yearCheapestAirlineMap
	 * @return Map<String, Integer>
	 */
	private static Map<String, Integer> populateCheapestAirlineCountMap(Map<Integer, String> yearCheapestAirlineMap) {
		Map<String, Integer> airlineCountMap = new HashMap<String, Integer>();
		for (Entry<Integer, String> entry : yearCheapestAirlineMap.entrySet()) {
			String airline = entry.getValue();

			Integer airlineCount = airlineCountMap.get(airline);
			if (airlineCount == null) {
				airlineCountMap.put(airline, 1);
			} else {
				airlineCountMap.put(airline, airlineCountMap.get(airline).intValue() + 1);
			}
		}
		return airlineCountMap;
	}
	
	/**
	 * Takes the predictedData from the JavaRDD, converts into a  Flight object
	 * and adds the data in the two input map
	 * @param yearMinPredictedPriceMap
	 * @param yearCheapestAirlineMap
	 * @param predictionList
	 * @return 
	 */
	private static void populateMaps(Map<Integer, Double> yearMinPredictedPriceMap,
			Map<Integer, String> yearCheapestAirlineMap, List<String> predictionList) throws IOException {
		final CSVParser parser = new CSVParser(',', '"');
		for (String prediction : predictionList) {
			String[] flightData = parser.parseLine(prediction);
			String airline = flightData[0];
			int year = Integer.parseInt(flightData[1].trim().replaceAll("\"", ""));
			double predictedPrice = Double.parseDouble(flightData[2]);

			Double minPredictedPrice = yearMinPredictedPriceMap.get(year);
			if (minPredictedPrice == null) {
				yearMinPredictedPriceMap.put(year, predictedPrice);
				yearCheapestAirlineMap.put(year, airline);
			} else {
				if (predictedPrice < minPredictedPrice) {
					yearMinPredictedPriceMap.put(year, predictedPrice);
					yearCheapestAirlineMap.put(year, airline);
				}
			}
		}
	}
}
