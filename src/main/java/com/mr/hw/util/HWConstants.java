package com.mr.hw.util;
/**
 * @author kamlendrak
 *
 */
public class HWConstants {

	public static final int YEAR = 0;
	public static final int QUARTER =1;
	public static final int MONTH = 2;
	public static final int DAY_OF_MONTH = 3;
	public static final int DAY_OF_WEEK = 4;
	public static final int FL_DATE = 5;
	public static final int UNIQUE_CARRIER = 6;
	public static final int AIRLINE_ID = 7;
	public static final int CARRIER = 8;
	public static final int TAIL_NUM = 9;
	public static final int FL_NUM = 10;
	
	public static final int ORIGIN_AIRPORT_ID = 11;
	public static final int ORIGIN_AIRPORT_SEQ_ID = 12;
	public static final int ORIGIN_CITY_MARKET_ID = 13;
	public static final int ORIGIN = 14;
	public static final int ORIGIN_CITY_NAME = 15;
	public static final int ORIGIN_STATE_ABR = 16;
	public static final int ORIGIN_STATE_FIPS = 17;
	public static final int ORIGIN_STATE_NM = 18;
	public static final int ORIGIN_WAC = 19;
	
	public static final int DEST_AIRPORT_ID = 20;
	public static final int DEST_AIRPORT_SEQ_ID = 21;
	public static final int DEST_CITY_MARKET_ID = 22;
	public static final int DEST = 23;
	public static final int DEST_CITY_NAME = 24;
	public static final int DEST_STATE_ABR = 25;
	public static final int DEST_STATE_FIPS = 26;
	public static final int DEST_STATE_NM = 27;
	public static final int DEST_WAC = 28;
	
	public static final int CRS_DEP_TIME = 29;
	public static final int DEP_TIME = 30;
	public static final int DEP_DELAY = 31;
	public static final int DEP_DELAY_NEW = 32;
	public static final int DEP_DEL15 = 33;
	public static final int DEP_DEL_GROUP = 34;
	public static final int DEP_TIME_BLK = 35;
	public static final int TAXI_OUT = 36;
	public static final int WHEELS_OFF = 37;
	public static final int WHEELS_ON = 38;
	public static final int TAXI_IN = 39;
	
	public static final int CRS_ARR_TIME = 40;
	public static final int ARR_TIME = 41;
	public static final int ARR_DELAY = 42;
	public static final int ARR_DELAY_NEW = 43;
	public static final int ARR_DEL15 = 44;
	public static final int ARR_DELAY_GROUP = 45;
	public static final int ARR_TIME_BLK = 46;
	public static final int CANCELLED = 47;
	public static final int CANCELLATION_CODE = 48;
	public static final int DIVERTED = 49;
	public static final int CRS_ELAPSED_TIME = 50;
	public static final int ACTUAL_ELAPSED_TIME = 51;
	public static final int AIR_TIME = 52;
	public static final int FLIGHTS = 53;
	public static final int DISTANCE = 54;
	public static final int DISTANCE_GROUP = 55;
	
	public static final int CARRIER_DELAY = 56;
	public static final int WEATHER_DELAY = 57;
	public static final int NAS_DELAY = 58;
	public static final int SECURITY_DELAY = 59;
	public static final int LATE_AIRCRAFT_DELAY = 60;
	public static final int FIRST_DEP_TIME = 61;
	public static final int TOTAL_ADD_GTIME = 62;
	public static final int LONGEST_ADD_GTIME = 63;
	
	public static final int DIV_AIRPORT_LANDINGS = 64;
	public static final int DIV_REACHED_DEST = 65;
	public static final int DIV_ACTUAL_ELAPSED_TIME = 66;
	public static final int DIV_ARR_DELAY = 67;
	public static final int DIV_DISTANCE = 68;
	
	public static final int DIV1_AIRPORT = 69;
	public static final int DIV1_AIRPORT_ID = 70;
	public static final int DIV1_AIRPORT_SEQ_ID = 71;
	public static final int DIV1_WHEELS_ON = 72;
	public static final int DIV1_TOTAL_GTIME = 73;
	public static final int DIV1_LONGEST_GTIME = 74;
	public static final int DIV1_WHEELS_OFF = 75;
	public static final int DIV1_TAIL_NUM = 76;
	
	public static final int DIV2_AIRPORT = 77;
	public static final int DIV2_AIRPORT_ID = 78;
	public static final int DIV2_AIRPORT_SEQ_ID = 89;
	public static final int DIV2_WHEELS_ON = 80;
	public static final int DIV2_TOTAL_GTIME = 81;
	public static final int DIV2_LONGEST_GTIME = 82;
	public static final int DIV2_WHEELS_OFF  = 83;
	public static final int DIV2_TAIL_NUM = 84;
	
	public static final int DIV3_AIRPORT = 85;
	public static final int DIV3_AIRPORT_ID = 86;
	public static final int DIV3_AIRPORT_SEQ_ID = 87;
	public static final int DIV3_WHEELS_ON = 88;
	public static final int DIV3_TOTAL_GTIME = 89;
	public static final int DIV3_LONGEST_GTIME = 90;
	public static final int DIV3_WHEELS_OFF  = 91;
	public static final int DIV3_TAIL_NUM = 92;
	
	public static final int DIV4_AIRPORT = 93;
	public static final int DIV4_AIRPORT_ID = 94;
	public static final int DIV4_AIRPORT_SEQ_ID = 95;
	public static final int DIV4_WHEELS_ON = 96;
	public static final int DIV4_TOTAL_GTIME = 97;
	public static final int DIV4_LONGEST_GTIME = 98;
	public static final int DIV4_WHEELS_OFF  = 99;
	public static final int DIV4_TAIL_NUM = 100;
	
	public static final int DIV5_AIRPORT = 101;
	public static final int DIV5_AIRPORT_ID = 102;
	public static final int DIV5_AIRPORT_SEQ_ID = 103;
	public static final int DIV5_WHEELS_ON = 104;
	public static final int DIV5_TOTAL_GTIME = 105;
	public static final int DIV5_LONGEST_GTIME = 106;
	public static final int DIV5_WHEELS_OFF  = 107;
	public static final int DIV5_TAIL_NUM = 108;
	
	public static final int AVG_TICKET_PRICE = 109;
	
	public static final String SEPERATOR_COMMA = ",";
	public static final String SEPERATOR_UNDERSCORE = "_";
	public static final String MODEL_PATH = "models";
	public static final String VALIDATION_FILE = "validation";
	public static final String REQUEST_FILE = "request";
}
