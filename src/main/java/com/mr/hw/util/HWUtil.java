package com.mr.hw.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import org.apache.hadoop.io.DoubleWritable;

/**
 * Utility class for the project.
 * @author kamlendra, Mayuri, Sri Annapurna, Kavya
 *
 */
public class HWUtil {
	
	/**
	 * Checks if the string is empty or null
	 * @param value
	 * @return
	 */
	public static boolean isStringEmptyOrNull(String value) {
		return value == null || value.trim().length() == 0;
	}
	
	/**
	 * Returns the value of argument opt.
	 * @param opt
	 * @param args
	 * @return
	 */
	public static String getOpt(String opt, String[] args) {
		opt = "-" + opt + "=";
		for (int i = 0; i < args.length; i++)
			if (args[i].startsWith(opt)) {
				String value = args[i].replaceAll(opt, "");
				args[i] = args[0];
				args = Arrays.copyOfRange(args, 1, args.length);
				return value;
			}
		return null;
	}
	
	/**
	 * Calculates the average price for a carrier from a list of price.
	 * @param priceList price list of a carrier.
	 * @return
	 */
	public static double calculateAveragePrice(List<Double> priceList) {
		double sum = 0;
		for(Double price : priceList) {
			sum+= price;
		}
		return sum / priceList.size();
	}
	
	/**
	 * Calculates the median of the price list.
	 * @param priceList
	 * @return
	 */
	public static double calculateMedian(List<Double> priceList) {
		if(priceList == null || priceList.size() == 0) {
			return Double.NaN;
		}
		Collections.sort(priceList);
		if(priceList.size() % 2 == 1) { // odd number of elements in list. return middle element
			return priceList.get(priceList.size() / 2);
		} else { // even number of elements in the list. return average of middle elements.
			return (priceList.get(priceList.size() / 2) + priceList.get((priceList.size()/2) + 1)) / 2;
		}
	}

	public static double calculateFastMedian(List<Double> values) {
		return getNthElement(values.toArray(new Double[values.size()]), values.size()/2);
	}
	
	
	private static double getNthElement(Double[] elements, int k) {
		 if (elements == null || elements.length <= k)
		  throw new Error();
		 
		 int from = 0, to = elements.length - 1;
		 
		 // if from == to we reached the kth element
		 while (from < to) {
		  int r = from, w = to;
		  double mid = elements[(r + w) / 2];
		 
		  // stop if the reader and writer meets
		  while (r < w) {
		 
		   if (elements[r] >= mid) { // put the large values at the end
		    double tmp = elements[w];
		    elements[w] = elements[r];
		    elements[r] = tmp;
		    w--;
		   } else { // the value is smaller than the pivot, skip
		    r++;
		   }
		  }
		 
		  // if we stepped up (r++) we need to step one down
		  if (elements[r] > mid)
		   r--;
		 
		  // the r pointer is on the end of the first k elements
		  if (k <= r) {
		   to = r;
		  } else {
		   from = r + 1;
		  }
		 }
		 
		 return elements[k];
		}
	
	/**
	 * Returns the list of doubles corresponding to List of double writables.
	 * @param values
	 * @return
	 */
	public static List<Double> getDoubleList(Iterable<DoubleWritable> values) {
		List<Double> result = new ArrayList<Double>();
		for(DoubleWritable value : values) {
			result.add(value.get());
		}
		return result;
	}
	
	/**
	 * Converts date to String.
	 * @param cal
	 * @return
	 */
	public static String convertDateToString(Calendar cal) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
		String date = sdf.format(cal.getTime());
		return date;
	}
	
	
	/**
	 * Converts the string date representation to date.
	 * @param dateString
	 * @return
	 * @throws ParseException
	 */
	public static Date convertStringToDate(String dateString) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
		Date date = sdf.parse(dateString);
		return date;
	}
	
	/**
	 * Converts the given date in string format to date object. resets the timestamp.
	 * @param dateString
	 * @return
	 * @throws ParseException
	 */
	public static Date convertStringToDateWithoutTimeStamp(String dateString) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
		Date date = sdf.parse(dateString);
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0); 
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}
	
	/**
	 * Removes the hour, minute and seconds from the given date.
	 * @param date
	 * @return
	 */
	public static Date removeTimeStamp(final Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0); 
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}
	
	/**
	 * Calculates time difference between two dates.
	 * @param arrTimeString
	 * @param deptTimeString
	 * @return
	 * @throws ParseException
	 */
	public static long getTimeDifferenceInMilis(String arrTimeString, String deptTimeString) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
		Date arrTime = sdf.parse(arrTimeString);
		Date deptTime = sdf.parse(deptTimeString);
		return deptTime.getTime() - arrTime.getTime();
	}
	
	/**
	 * Calculates time difference between two dates.
	 * @param arrTimeString
	 * @param deptTimeString
	 * @return
	 * @throws ParseException
	 */
	public static long getTimeDifferenceInMilis(Date arrTime, Date deptTime) throws ParseException {
		return deptTime.getTime() - arrTime.getTime();
	}
	
	/**
	 * Returns the next date for a given day.
	 * @param d
	 * @return
	 */
	public static Date getNextDate(Date d) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(d);
		cal.add(Calendar.DAY_OF_MONTH, 1);
		Date date = cal.getTime();
		return removeTimeStamp(date);
	}	
	
	/**
	 * Checks if timeString if of form 040. Converts it to 0040.
	 * @param timeString
	 * @return
	 */
	public static String checkTimeFormat(String timeString) {
		if(timeString.length() == 3) {
			return "0".concat(timeString); 
		}
		if(timeString.length() == 2) {
			return "00".concat(timeString);
		}
		if(timeString.length() == 1) {
			return "000".concat(timeString);
		}
		return timeString;
	}
	
	public static String checkDateFormat(String dateString) {
		return dateString.length() == 1 ? "0".concat(dateString) : dateString;
	}
	
	public static String getDateString(String[] flightData, int timeColumnIndex) {
		StringBuilder builder = new StringBuilder();
		builder.append(flightData[HWConstants.YEAR] + ".");
		builder.append(checkDateFormat(flightData[HWConstants.MONTH])+ ".");
		builder.append(checkDateFormat(flightData[HWConstants.DAY_OF_MONTH])+ " ");
		String timeString = checkTimeFormat(flightData[timeColumnIndex]);
		if(timeString == null || timeString.length() == 0) {
			builder.append("NA");
		} else {
			builder.append(timeString.substring(0, 2) + ":");
			builder.append(timeString.substring(2, 4) + ":");
			builder.append("00");
		}
		return builder.toString();
	}
	
	/**
	 * 
	 * @param flight
	 * @return String
	 */
	public static String prepareRequestKey(String[] tokens) {
		StringBuilder sb = new StringBuilder();
		sb.append(tokens[1] + HWConstants.SEPERATOR_COMMA);
		sb.append(tokens[2] + HWConstants.SEPERATOR_COMMA);
		sb.append(tokens[3] + HWConstants.SEPERATOR_COMMA);
		sb.append(tokens[6] + HWConstants.SEPERATOR_COMMA);
		sb.append(tokens[12]);
		return sb.toString();
	}
	
	/**
	 * 
	 * @return
	 */
	public static int[] getFeatureIndexes() {
		return new int[] {2,3,4,7,8,10,13,14,15,16};
	}
}