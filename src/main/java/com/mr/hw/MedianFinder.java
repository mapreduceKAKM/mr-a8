package com.mr.hw;

import java.util.ArrayList;
import java.util.List;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFunction;

import com.mr.hw.flight.FlightDTO;
import com.mr.hw.util.HWConstants;
import com.mr.hw.util.HWUtil;

import scala.Tuple2;

/**
 * Class to find the median price for every week of an airline.
 * @author kamlendra, keskarm, AnnuRaju, Kavya
 */
public class MedianFinder {

	/**
	 * Finds and saves the median ticket price / week for the given airline.
	 * @param jsc
	 * @param filteredSaneFlights
	 * @param cheapestAirline
	 * @param outputPath
	 */
	public static void findMedianPrice(JavaSparkContext jsc, JavaRDD<FlightDTO> filteredSaneFlights,
			final String cheapestAirline, String outputPath) {

		JavaRDD<FlightDTO> cheapestFlightInfo = filteredSaneFlights.filter(new Function<FlightDTO, Boolean>() {
			private static final long serialVersionUID = 1L;

			@Override
			public Boolean call(FlightDTO flight) throws Exception {
				return flight.getAirline().equalsIgnoreCase(cheapestAirline);
			}
		});

		JavaPairRDD<String, Iterable<Double>> mapped = cheapestFlightInfo
				.mapToPair(new PairFunction<FlightDTO, String, Double>() {
					private static final long serialVersionUID = 1L;

					@Override
					public Tuple2<String, Double> call(FlightDTO flight) throws Exception {
						return new Tuple2<String, Double>(
								flight.getYear() + HWConstants.SEPERATOR_COMMA + flight.getWeekOfYear(),
								flight.getTicketPrice());
					}
				}).groupByKey();

		JavaRDD<String> output = mapped.map(new Function<Tuple2<String, Iterable<Double>>, String>() {
			private static final long serialVersionUID = 1L;

			@Override
			public String call(Tuple2<String, Iterable<Double>> tuple) throws Exception {
				List<Double> priceList = new ArrayList<Double>();
				for (Double price : tuple._2()) {
					priceList.add(price);
				}
				double median = HWUtil.calculateMedian(priceList);
				return cheapestAirline + HWConstants.SEPERATOR_COMMA + tuple._1() + HWConstants.SEPERATOR_COMMA
						+ median;
			}
		});
		output.saveAsTextFile(outputPath);
	}
}