

N=1
PSEUDO_INPUT=/Users/kamlendrak/Documents/Studies/Spring-2016/Map-Reduce/Homeworks/Data/
PSEUDO_OUTPUT=Output/N-${N}

AWS_REGION=us-west-2a
AWS_INSTANCE_TYPE=m3.xlarge
AWS_INSTANCE_COUNT=5
AWS_BUCKET_NAME=kamlendrak
AWS_OUTPUT_DIR=output
AWS_LOG_DIR=logs

MAIN_CLASS=com.mr.hw.driver.A8
AWS_INPUT_PATH=s3n://mrclassvitek/data/
AWS_OUTPUT_PATH=s3://${AWS_BUCKET_NAME}/${AWS_OUTPUT_DIR}/N-${N}
AWS_OUTPUT_LOCAL=Output/N-${N}

format: 
	hdfs namenode -format

unsafe:
	hdfs dfsadmin -safemode leave

terminate:	
	aws emr terminate-clusters --cluster-ids `cat clusterId.txt`

hstart:
	start-dfs.sh
	start-yarn.sh
	mr-jobhistory-daemon.sh start historyserver

hstop:
	mr-jobhistory-daemon.sh stop historyserver 
	stop-yarn.sh
	stop-dfs.sh

report:
	Rscript -e "rmarkdown::render('A8Report.Rmd')" ${N}

cloud-setup:
	aws s3 mb s3://${AWS_BUCKET_NAME}

cloud:
	mvn clean install
	cp target/Homework8.jar .
	aws s3 cp Homework8.jar s3://${AWS_BUCKET_NAME}  
	aws s3 cp Spark-Properties.json s3://${AWS_BUCKET_NAME} --grants full=uri=http://acs.amazonaws.com/groups/global/AllUsers
	aws emr create-cluster --name "A8 - Linear Regression" --release-label emr-4.4.0 --applications Name=Spark --configurations https://${AWS_BUCKET_NAME}.s3.amazonaws.com/Spark-Properties.json --instance-groups InstanceGroupType=Master,InstanceCount=1,InstanceType=${AWS_INSTANCE_TYPE} InstanceGroupType=CORE,InstanceCount=${AWS_INSTANCE_COUNT},InstanceType=${AWS_INSTANCE_TYPE} --steps Type=Spark,Name="A8 - Spark Model",ActionOnFailure=CONTINUE,Args=[--deploy-mode,cluster,--class,${MAIN_CLASS},s3n://${AWS_BUCKET_NAME}/Homework8.jar,${AWS_INPUT_PATH},${AWS_OUTPUT_PATH},${N}] --log-uri s3://${AWS_BUCKET_NAME}/${AWS_LOG_DIR} --service-role EMR_DefaultRole --ec2-attributes InstanceProfile=EMR_EC2_DefaultRole,AvailabilityZone=${AWS_REGION} --enable-debugging > clusterId.txt
	sleep 60;
	sh clusterWaitingCheck.sh `cat clusterId.txt`
	aws s3 cp ${AWS_OUTPUT_PATH} ${AWS_OUTPUT_LOCAL} --recursive
	aws emr terminate-clusters --cluster-ids `cat clusterId.txt`
	Rscript -e "rmarkdown::render('A8Report.Rmd')" ${AWS_OUTPUT_LOCAL} ${N}

pseudo:
	mvn clean install
	spark-submit --master local --class ${MAIN_CLASS} target/Homework8.jar ${PSEUDO_INPUT} ${PSEUDO_OUTPUT} ${N}
	Rscript -e "rmarkdown::render('A8Report.Rmd')" ${PSEUDO_OUTPUT} ${N}


	