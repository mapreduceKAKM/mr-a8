#!/bin/sh


clusterid=$1
echo $clusterid
clusterStatus() {
	value=`aws emr list-clusters --cluster-states WAITING --query "Clusters[*].Id" --output text`
	if [ -w $value ]
	then
	 return 0
	else
	 return 1
	fi
}

while clusterStatus $1
do
sleep 20
done
echo "Complete"
